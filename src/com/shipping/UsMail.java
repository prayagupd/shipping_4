package com.shipping;

public class UsMail extends AbstractCarrierImpl {

	@Override
	public String getType(){
		return "UsMail";
	}
	
	@Override
	public void calculateCharge(Package packageToSend) {
		double charge = 0.0;
		if(packageToSend.getWeight()<3)
			charge = (1);
		else 
			charge = (0.55*packageToSend.getWeight());
		setCharge(charge);
		calculateDiscount(packageToSend);
		setCharge(charge - getDiscount());
		System.out.println(getType() + " charge = " + getCharge());
	}

}
