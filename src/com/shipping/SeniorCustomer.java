package com.shipping;

public class SeniorCustomer extends AbstractCustomer {

	public SeniorCustomer(String name) {
		super(name);
		setDiscount();
	}

	public void setDiscount() {
		setDiscount(15.0);
	}
	
}
