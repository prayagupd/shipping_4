package com.shipping;

public interface Carrier {
	
	public String getType();
	
	public void calculateCharge(Package packageToSend);

	public void calculateDiscount(Package packageToSend);
	
	public double getCharge();
	
	public double getDiscount();
	
	public void setDiscount(double discount);
}
