package com.shipping;

public abstract class AbstractCustomer {
	private String name;
	protected Double discount;
	
	AbstractCustomer(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	
}
