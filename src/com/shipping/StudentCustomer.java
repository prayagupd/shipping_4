package com.shipping;

public class StudentCustomer extends AbstractCustomer {

	
	public StudentCustomer(String name) {
		super(name);
		setDiscount();
	}

	public void setDiscount() {
		setDiscount(10.0);
	}
	
}
