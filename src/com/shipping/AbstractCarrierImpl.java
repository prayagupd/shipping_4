package com.shipping;

public abstract class AbstractCarrierImpl implements Carrier {
	private double charge;
	private double discount;
	
	@Override
	public double getCharge() {
		return charge;
	}


	protected void setCharge(double charge) {
		this.charge = charge;
	}

	
	public double getDiscount() {
		return discount;
	}


	public void setDiscount(double discount) {
		this.discount = discount;
	}


	@Override
	public abstract void calculateCharge(Package packageToSend);

	@Override
	public void calculateDiscount(Package packageToSend) {
		double discount = getCharge() * (packageToSend.getCustomer().getDiscount()/100);
		//System.out.println("Discount for " + getCharge() + " = " + discount);
		setDiscount(discount);
	}
	
}
