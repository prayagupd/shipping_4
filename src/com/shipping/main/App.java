package com.shipping.main;
import com.shipping.*;
import com.shipping.CarrierStrategy;
import com.shipping.Carrier;
import com.shipping.CarrierStrategyImpl;
import com.shipping.Package;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
	public static void main(String[] args) throws Exception {
		System.out.println("Enter package desc : ");
		final String name = getString();
		
		System.out.println("Enter package weight : ");
		final Double weight = getDouble();
		
		System.out.println("Enter package zone : ");
		final String zone = getString();
		
		AbstractCustomer student = new StudentCustomer("Pray");
		Package packageToSend =  new Package(name, weight, zone, student);
		
		CarrierStrategy s = new CarrierStrategyImpl();
		Carrier carrier = s.send(packageToSend);
	
		//Baby stroller		$20.00		FedEx
		System.out.println( packageToSend.getDesc()  + " " + carrier.getCharge() + " " + carrier.getType());
	}
	
	// reads an double from the keyboard input
	public static double getDouble() throws IOException  {
		String s = getString();
		return (Double.valueOf(s)).doubleValue();
    }
	
 //reads a string from the keyboard input
   public static String getString() throws IOException {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
   }
}
