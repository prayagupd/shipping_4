package com.shipping;

public class CarrierStrategyImpl implements CarrierStrategy {
	
	Carrier carrier = null;
	
	@Override
	public Carrier send(Package packageToSend) {
		Carrier carrier = findLowestPrice(packageToSend);
		return carrier;
	}

	@Override
	public Carrier findLowestPrice(Package packageToSend) {
		Carrier ups = new UPS();
		ups.calculateCharge(packageToSend);
		carrier = ups;
		
		Carrier usMail = new UsMail();
		usMail.calculateCharge(packageToSend);
		
		if(usMail.getCharge() < carrier.getCharge())
			carrier = usMail;
		
		Carrier fedEx = new FedEx();
		fedEx.calculateCharge(packageToSend);
		
		if(fedEx.getCharge() < carrier.getCharge())
			carrier = fedEx;
		
		return carrier;
	}

}
