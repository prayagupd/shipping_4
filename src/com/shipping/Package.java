package com.shipping;

public class Package {
	private String desc;
	private double weight;
	private String zone;
	private AbstractCustomer customer;
	
	public Package(String desc, double weight, String zone, AbstractCustomer customer) {
		super();
		this.desc = desc;
		this.weight = weight;
		this.zone = zone;
		this.customer = customer;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public double getWeight() {
		return weight;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}

	public AbstractCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(AbstractCustomer customer) {
		this.customer = customer;
	}
	
	
}
