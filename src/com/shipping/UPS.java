package com.shipping;

public class UPS extends AbstractCarrierImpl {
	
	@Override
	public String getType(){
		return "UPS";
	}
	
	@Override
	public void calculateCharge(Package packageToSend) {
		double charge = packageToSend.getWeight() * 0.45;
		setCharge(charge);
		calculateDiscount(packageToSend);
		setCharge(charge - getDiscount());
		System.out.println(getType() + " charge = " + getCharge());
	}


}
