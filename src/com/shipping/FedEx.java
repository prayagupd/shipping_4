package com.shipping;

import java.util.HashMap;

public class FedEx extends AbstractCarrierImpl {
	
	private static HashMap<String, Double> zoneCharges = new HashMap<String, Double>(){
		private static final long serialVersionUID = 1L;

	{
		put("IA", 0.35);
		put("MT", 0.35);
		put("OR", 0.35);
		put("CA", 0.35);
		
		put("TX", 0.30);
		put("UT", 0.30);
		
		put("FL", 0.35);
		put("MA", 0.35);
		put("OH", 0.35);
		
		put("OTHERS", 0.43);
	}};
	
	@Override
	public String getType(){
		return "FedEx";
	}
	
	@Override
	public void calculateCharge(Package packageToSend) {
		double charge = 0.0;
		if(zoneCharges.containsKey(packageToSend.getZone()))
			charge = (packageToSend.getWeight() * zoneCharges.get(packageToSend.getZone()));
		else 
			charge = (packageToSend.getWeight() * zoneCharges.get("OTHERS"));
		setCharge(charge);
		calculateDiscount(packageToSend);
		setCharge(charge - getDiscount());
		
		System.out.println(getType() + " charge = " + getCharge());
	}

}
