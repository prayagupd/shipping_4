package com.shipping;

public interface CarrierStrategy {
	public Carrier send(Package packageToSend);
	public Carrier findLowestPrice(Package packageToSend);
}
